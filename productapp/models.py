from django.db import models
from users.models import DatabaseList
# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    sku = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    database = models.ForeignKey(DatabaseList, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
 