from django.shortcuts import render , redirect
from .form import ProductForm
from users.models import Userdb , DatabaseList
from .models import Product
from django.contrib import messages
from django.contrib.auth import logout


# Create your views here.

def home(request):

    if request.method == 'POST':
        form = ProductForm(request.POST)
        if form.is_valid():
            database = form.cleaned_data['database']
            # database_id = DatabaseList.objects.get(name = database)
            name = form.cleaned_data['name']
            description = form.cleaned_data['description']
            sku = form.cleaned_data['sku']
            price = form.cleaned_data['price']
            product  = Product(name = name , description = description , sku = sku , price = price , database = database)
            product.save(using= str(database)) 
            messages.info(request, 'Product created sucessfully')
            return render( request , 'product/products.html' )
        else:
            form = ProductForm()
            return render(request , 'product/home.html' , {'form' : form})
    else:
        # import pdb ; pdb.set_trace()
        form = ProductForm()
        return render(request , 'product/home.html' , {'form' : form})
