from django.forms import ModelForm
from django import forms
from users.models import DatabaseList
from . models import Product


class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = ['database','name', 'description', 'sku' , 'price'  ]
# class ProductForm(forms.Form):
#     name = forms.CharField(max_length=100)
#     description = forms.CharField(widget=forms.Textarea)
#     sku = forms.CharField(max_length=100)
#     price = forms.IntegerField()
#     database_list  = forms.ModelMultipleChoiceField(queryset=DatabaseList.objects.all())
    
