About Project :-
    This is a multi Database Project where alsomost 6 database's 2 are in Postgres and 4 are in mysql database 5 databse which can select by user and create product on that particular database.
    There is One default database for user and all other database activites.


    This is the demo application which is having the two role 
    1. Admin
    2. User
        Admin can manage the database and user can register on the application when he register he got an email which contain the Username name password So the user can essaly login into the System.

    Setup Instructions :-
        1.  Create a vertual Envernment for the project
                virtualenv -p python3 venv
        2.  Activate the Python Vartual Enverment
                source venv/bin/activate
        3.  Install Django with the following command
                pip install django
        4.  Clone the repo of git hub 
                
                Clone with SSH
                git@gitlab.com:jeevan-carpenter/multidbproject.git

                Clone with HTTPS
                https://gitlab.com/jeevan-carpenter/multidbproject.git
                
        5.  Create database for the project 
                postgerss :-
                        database1
                        database2
                mysql :-
                        default
                        database3
                        database4
                        database5
        6.  Create migrations for the Project :-
                python3 manage.py makemigrations
        7.  Create table stucture with the following command :-
                python3 manage.py migrate
                python3 manage.py migrate --database= database1 ,2,3,4,5
        8.  Run the Server
                python3 manage.py runserver
